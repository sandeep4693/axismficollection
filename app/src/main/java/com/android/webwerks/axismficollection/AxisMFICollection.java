package com.android.webwerks.axismficollection;

import android.app.Application;

import com.android.webwerks.axismficollection.dagger.AppComponent;
import com.android.webwerks.axismficollection.dagger.DaggerAppComponent;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AxisMFICollection extends Application {

    private static AxisMFICollection INSTANCE;
    private AppComponent mAppComponent;

    public static AxisMFICollection getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new AxisMFICollection();
        }
        return INSTANCE;
    }

    public Retrofit getApiClient() {
        return new Retrofit.Builder().addConverterFactory(GsonConverterFactory.create()).build();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        INSTANCE = this;
    }

    public AppComponent getComponent() {
        if (mAppComponent == null) {
            mAppComponent = DaggerAppComponent.builder().build();
        }
        return mAppComponent;
    }
}
