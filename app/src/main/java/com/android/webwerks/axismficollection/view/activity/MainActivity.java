package com.android.webwerks.axismficollection.view.activity;

import android.os.Bundle;
import android.widget.Toast;

import com.android.webwerks.axismficollection.AxisMFICollection;
import com.android.webwerks.axismficollection.R;
import com.android.webwerks.axismficollection.presenter.LoginPresenter;
import com.android.webwerks.axismficollection.viewinterfaces.LoginView;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends BaseActivity implements LoginView {

    private static final String TAG = MainActivity.class.getSimpleName();
    @Inject
    LoginPresenter mLoginPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        AxisMFICollection.getInstance().getComponent().inject(this);
        mLoginPresenter.registerView(this);
        ButterKnife.bind(this);
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(MainActivity.this, message, Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.btn_hello)
    public void onButtonClick() {
        mLoginPresenter.showAlertMessage();
    }
}
