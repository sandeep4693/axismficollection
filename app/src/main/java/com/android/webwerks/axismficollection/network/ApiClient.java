package com.android.webwerks.axismficollection.network;

import com.android.webwerks.axismficollection.AxisMFICollection;

public class ApiClient {

    private static ApiClient INSTANCE;

    public static ApiClient getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new ApiClient();
        }
        return INSTANCE;
    }

    public ApiInterface getApiClient() {
        return AxisMFICollection.getInstance().getApiClient().create(ApiInterface.class);
    }
}
