package com.android.webwerks.axismficollection.presenter;

import com.android.webwerks.axismficollection.viewinterfaces.LoginView;

public class LoginPresenterImpl implements LoginPresenter {

    private LoginView mLoginView;

    @Override
    public void registerView(LoginView view) {
        mLoginView = view;
    }

    @Override
    public void showAlertMessage() {
        mLoginView.showMessage("Alert message");
    }
}
