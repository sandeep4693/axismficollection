package com.android.webwerks.axismficollection.viewinterfaces;

public interface BaseView {

    void showMessage(String message);

}
