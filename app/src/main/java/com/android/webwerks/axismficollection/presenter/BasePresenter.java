package com.android.webwerks.axismficollection.presenter;

import com.android.webwerks.axismficollection.viewinterfaces.BaseView;

public interface BasePresenter<T extends BaseView> {

    void registerView(T view);
}
