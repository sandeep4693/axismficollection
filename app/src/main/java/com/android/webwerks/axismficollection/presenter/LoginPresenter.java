package com.android.webwerks.axismficollection.presenter;

import com.android.webwerks.axismficollection.viewinterfaces.LoginView;

public interface LoginPresenter extends BasePresenter<LoginView> {

    void showAlertMessage();

}
