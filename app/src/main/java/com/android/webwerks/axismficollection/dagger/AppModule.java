package com.android.webwerks.axismficollection.dagger;

import com.android.webwerks.axismficollection.presenter.LoginPresenter;
import com.android.webwerks.axismficollection.presenter.LoginPresenterImpl;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    @Provides
    LoginPresenter getLoginPresenter() {
        return new LoginPresenterImpl();
    }

}
