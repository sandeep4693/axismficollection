package com.android.webwerks.axismficollection.dagger;

import com.android.webwerks.axismficollection.view.activity.MainActivity;

import dagger.Component;

@Component(modules = AppModule.class)
public interface AppComponent {

    void inject(MainActivity mainActivity);

}
